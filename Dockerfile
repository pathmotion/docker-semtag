FROM alpine:3.12

# hadolint ignore=DL3018
RUN apk add --no-cache \
  bash \
  git \
  openssh-client

COPY semtag /usr/local/bin/semtag

CMD ["semtag", "--help"]
